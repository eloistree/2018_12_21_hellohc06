﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardListening : MonoBehaviour
{
    public float m_timeOne=4f;
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha0))
        {
            FireworkHC06.SetPinOnOffAll(0, m_timeOne);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            FireworkHC06.SetPinOnOffAll(1, m_timeOne);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            FireworkHC06.SetPinOnOffAll(2, m_timeOne);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            FireworkHC06.SetPinOnOffAll(3, m_timeOne);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            FireworkHC06.SetPinOnOffAll(4, m_timeOne);
        }
        else if(Input.GetKeyDown(KeyCode.Alpha5))
        {
            FireworkHC06.SetPinOnOffAll(5, m_timeOne);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            FireworkHC06.SetPinOnOffAll(6, m_timeOne);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha7))
        {
            FireworkHC06.SetPinOnOffAll(7, m_timeOne);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha8))
        {
            FireworkHC06.SetPinOnOffAll(8, m_timeOne);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha9))
        {
            FireworkHC06.SetPinOnOffAll(9, m_timeOne);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha0))
        {
            FireworkHC06.SetPinOnOffAll(0, m_timeOne);
        }
        else if (Input.GetKeyDown(KeyCode.A))
        {
            FireworkHC06.ActivateAllPins();
        }
        else if (Input.GetKeyDown(KeyCode.O))
        {
            FireworkHC06.DeactivateAllPins();
        }



    }
}
